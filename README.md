# Cypress_presta

To practice some Cypress and JS/TS skills this UI functional automation project was started. I did not have any specific plan for automating the test cases. Came with the approach that the crazier the tests the better for me - great opportunity to practice. So any kind of scenario can be found here.

# To start

1. Setup the Prestashop to work in your local environment. Feel free to use the docker containers setup from here: [Prestashop docker repository](https://gitlab.com/noke1/presta_docker)

2. Download this `Cypress_presta` repository to your local machine

3. Navigate into the root folder of the project and run `npm install`

4. After the successful installation run `npx cypress open`. The Cypress window should pop up after a while.

Then use the Cypress app to browse and run the tests! :)
