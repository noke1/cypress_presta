/// <reference types="cypress" />

require('cypress-xpath')
const chai = require("chai-sorted"),
expect = chai.expect // preference and tested with expect
chai.use(require("chai-sorted"))

import { Address } from "./models/api/Address"
import { Customer } from "./models/api/Customer"

export { }

declare global {
    namespace Cypress {
        interface Chainable {
            /**
             * Command co create a customer via the api call. It is a normal customer not an Administration role. The customer.xml template is used
             * as well as API 'customers' endpoint
             */
            registerCustomerViaApiCall(customer: Customer): Chainable<Element>

            /**
             * Command to login as a given user
             */
            loginAs(customer: Customer): Chainable<Element>

            /**
             * Command to add a FULL address to the user account via an api call. It adds an address that contains all minimal required data on the UI
             */
            addFullAddressToUserViaApiCall(customer: Customer, address: Address): Chainable<Element>

            /**
             * Command to add a MINIMAL address to the user account via an api call. It adds an address that contains only minimal required data on the UI
             */
            addMinimalAddressToUserViaApiCall(customer: Customer, address: Address): Chainable<Element>

            /**
             * Login via an api call - to speed up the tests and skip the UI interaction when it is not needed
             */
            loginViaApiCall(customer: Customer): Chainable<Element>

            /**
             * Add a product to an already existing cart assigned to a user. Just a one product ATM. 
             */
            createCustomerCartWithProduct(customer: Customer): Chainable<Element>
        }
    }
}