import './e2e'

import navbarPage from '../pages/NavbarPage'
import signInPage from '../pages/SignInPage'

import customerEndpoint from './api/CustomerEndpoint'
import addressEndpoint from './api/AddressEndpoint'
import loginEndpoint from './api/LoginEndpoint'
import cartsEndpoint from './api/CartsEndpoint'

import { parseToXML } from './utils/parser'

import { Customer } from './models/api/Customer'
import { Address } from './models/api/Address'
import { Cart } from './models/api/Cart'

import { addProductToCart } from './providers/CartProvider'

/*
* Command co create a customer via the api call. It is a normal customer not an Administration role. The customer.xml template is used
* as well as API 'customers' endpoint.
*/
Cypress.Commands.add('registerCustomerViaApiCall', (customer: Customer) => {
    const customerXMLPayload = parseToXML(customer)
    customerEndpoint.addCustomer(String(customerXMLPayload))
})

/*
* Command co create a customer via the api call. It is a normal customer not an Administration role. The customer.xml template is used
* as well as API 'customers' endpoint.
*/
Cypress.Commands.add('addFullAddressToUserViaApiCall', (customer: Customer, address: Address) => {
    customerEndpoint.getCustomerIdByEmail(customer.email).then(customerId => {
        address.id_customer = Number(customerId)
        address.lastname = customer.lastname
        address.firstname = customer.firstname
        const customerXMLPayload = parseToXML(address)

        addressEndpoint.addAddressToCustomer(String(customerXMLPayload))
    })
})

/*
* Command co create a customer via the api call. It is a normal customer not an Administration role. The customer.xml template is used
* as well as API 'customers' endpoint. It adds an address that contains only minimal required data on the UI
*/
Cypress.Commands.add('addMinimalAddressToUserViaApiCall', (customer: Customer, address: Address) => {
    customerEndpoint.getCustomerIdByEmail(customer.email).then(customerId => {
        address.id_customer = Number(customerId)
        address.lastname = customer.lastname
        address.firstname = customer.firstname
        address.company = ''
        address.vat_number = ''
        address.address2 = ''
        address.phone = ''
        const customerXMLPayload = parseToXML(address)

        addressEndpoint.addAddressToCustomer(String(customerXMLPayload))
    })
})

/*
* Command to login as as user via UI
*/
Cypress.Commands.add('loginAs', (customer: Customer) => {
    cy.visit('/')
    navbarPage.goToSignIn()
    signInPage.loginAs(customer)
    navbarPage.getFullNameSpanText().should('equal', `${customer.firstname} ${customer.lastname}`)
})

/*
* Just a wrapper around the api call
*/
Cypress.Commands.add('loginViaApiCall', (customer: Customer) => {
    loginEndpoint.login(customer)
})


Cypress.Commands.add('createCustomerCartWithProduct', (customer: Customer) => {
    customerEndpoint.getCustomerIdByEmail(customer.email).then(customerId => {
        cartsEndpoint.getCartIdForCustomer(customerId).then(cartId => {
            const cart: Cart = addProductToCart(Number(customerId), Number(cartId))
            const cartXMLPayload = parseToXML(cart)
            console.log(cartXMLPayload)

            cartsEndpoint.editCartForCustomer(String(cartXMLPayload))
        })
    })
})
