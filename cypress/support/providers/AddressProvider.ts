import { faker } from '@faker-js/faker'

import { Address } from '../models/api/Address'

export const getRandomAddressFullData = (): Address => {
    return new Address({
        id_customer: 0,
        id_state: 0,
        id_country: 14,
        deleted: 0,
        firstname: 'fake firstname',
        lastname: 'fake lastname',
        address1: `${faker.address.street()}, ${faker.address.buildingNumber()}`,
        address2: faker.company.catchPhraseNoun(),
        postcode: faker.address.zipCode('##-###'),
        city: faker.address.city(),
        company: faker.company.name(),
        vat_number: `IE${faker.random.numeric(5, { allowLeadingZeros: true })}`,
        alias: faker.address.state(),
        phone: faker.phone.number('###-###-###')
    })
}
