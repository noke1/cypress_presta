import { faker } from '@faker-js/faker'

import { Customer } from '../models/api/Customer'
import { Group } from '../models/api/Group'
import { Groups } from '../models/api/Groups'
import { Associations } from '../models/api/Associations'

export const getRandomCustomer = (): Customer => {
    const groupId3 = new Group({id: 3})
    const groups = new Groups({groups: [groupId3]})
    const associations = new Associations({groups})

    return  new Customer({
        id_default_group: 3,
        passwd: faker.internet.password(),
        lastname: faker.name.lastName(),
        firstname: faker.name.firstName(),
        email: faker.internet.email(),
        id_gender: (Math.random() > 0.5) ? 0 : 1,
        active: 1,
        is_guest: 0,
        id_shop: 1,
        id_shop_group: 1,
        associations
    })
}