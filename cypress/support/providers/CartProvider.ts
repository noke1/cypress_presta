import { Associations } from "../models/api/Associations"
import { Cart } from "../models/api/Cart"
import { Cart_Row } from "../models/api/Cart_Row"
import { Cart_Rows } from "../models/api/Cart_Rows"

export const addProductToCart = (customerId: number, cartId: number): Cart => {
    const cartRow = new Cart_Row({id_product: 3, id_product_attribute: 13, quantity: 4})
    const cartRows = new Cart_Rows({cart_rows: [cartRow]})
    const associations = new Associations({cart_rows: cartRows})
    const cart = new Cart({
        id: cartId,
        id_currency: 1,
        id_customer: customerId,
        id_lang: 1,
        id_shop_group: 1,
        id_shop: 1,
        associations
    })
    return cart
}
