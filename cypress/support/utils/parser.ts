import * as xml2js from 'xml2js'

export const parseToXML = (object: Object): any => {
    const jsoned = JSON.parse(JSON.stringify(object, null, 3))
    const className: string = object.constructor.name.toLowerCase()
    const finalJson = {
        [className]: jsoned
    }
    const rootNodeName = 'prestashop'
    const builder = new xml2js.Builder({rootName: rootNodeName})
    return builder.buildObject(finalJson)
}