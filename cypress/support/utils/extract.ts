export const formatOrderConfirmation = (htmlText: string): string => {
    const formatOrderConfirmationRegex: RegExp = /\n|\s{2,}|[^a-zA-Z\s]/g
    return htmlText.replace(formatOrderConfirmationRegex, '')
}