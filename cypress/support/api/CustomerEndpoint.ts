class CustomerEndpoint {

    getCustomerIdByEmail(email: string) {
        return cy.request({
            method: 'GET',
            url: `api/customers?filter[email]=${email}`,
            headers: {
                'Authorization': `Basic ${Cypress.env('prestaApiKey')}`,
                'Content-Type': 'application/xml'
            },
        }).then(response => {
            expect(response.status).to.eq(200)
            cy.wrap(Cypress.$(response.body))
                .then(xml => {
                    const userId: string = xml.filter('prestashop').find('customers').find('customer').attr('id')
                    cy.log(`User ID obtained from the api: ***${userId}***`)
                    cy.wrap(userId)
                })
        })
    }

    addCustomer(requestBody: string) {
        cy.request({
            method: 'POST',
            url: 'api/customers',
            headers: {
                'Authorization': `Basic ${Cypress.env('prestaApiKey')}`,
                'Content-Type': 'application/xml'
            },
            body: requestBody
        }).then(response => {
            expect(response.status).to.eq(201)
        })
    }

}


export default new CustomerEndpoint()