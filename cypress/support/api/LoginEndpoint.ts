import { Customer } from "../models/api/Customer"

class LoginEndpoint {

    /*
    * Api call to login as a customer
    * back - the path that user will be redirected to after logging in
    * submitLogin - saw that this parameter is specified when logging in from UI so adding it here also :)
    */
    login(customer: Customer) {
        return cy.request({
            method: 'POST',
            url: '/login', // baseUrl will be prepended to this url
            form: true, // indicates the body should be form urlencoded and sets Content-Type: application/x-www-form-urlencoded headers
            body: {
                back: 'my-account',
                email: customer.email,
                password: customer.passwd,
                submitLogin: 1
            },
        }).then(response => {
            expect(response.status).to.eq(200)
        })
    }
}


export default new LoginEndpoint()