class AddressEndpoint {

    addAddressToCustomer(requestBody: string) {
        return cy.request({
            method: 'POST',
            url: 'api/addresses',
            headers: {
                'Authorization': `Basic ${Cypress.env('prestaApiKey')}`,
                'Content-Type': 'application/xml'
            },
            body: requestBody
        }).then(response => {
            expect(response.status).to.eq(201)
        })
    }
}


export default new AddressEndpoint()