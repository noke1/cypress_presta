class CartsEndpoint {

    getCartIdForCustomer(customerId) {
        return cy.request({
            method: 'GET',
            url: `api/carts?filter[id_customer]=${customerId}`,
            headers: {
                'Authorization': `Basic ${Cypress.env('prestaApiKey')}`,
                'Content-Type': 'application/xml'
            },
        }).then(response => {
            expect(response.status).to.eq(200)
            cy.wrap(Cypress.$(response.body))
                .then(xml => {
                    const cartId: string = xml.filter('prestashop').find('carts').find('cart').attr('id')
                    cy.log(`Cart ID obtained from the api: ***${cartId}***`)
                    cy.wrap(cartId)
                })
        })
    }

    editCartForCustomer(requestBody: string) {
        return cy.request({
            method: 'PUT',
            url: 'api/carts',
            headers: {
                'Authorization': `Basic ${Cypress.env('prestaApiKey')}`,
                'Content-Type': 'application/xml'
            },
            body: requestBody
        }).then(response => {
            expect(response.status).to.eq(200)
        })
    }

}


export default new CartsEndpoint()