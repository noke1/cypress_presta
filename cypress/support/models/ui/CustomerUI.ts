interface ICustomerUI {
    passwd: string
    lastname: string
    firstname: string
    email: string
    socialTitle: SocialTitle
    password: string
}

/**
 * Customer data compromised from the fields visible on the UI layer
 */
export class CustomerUI {
    private socialTitle: SocialTitle
    private firstname: string
    private lastname: string
    private email: string
    private password: string

    constructor(options: ICustomerUI) {
        this.socialTitle = options.socialTitle
        this.firstname = options.firstname
        this.lastname = options.lastname
        this.email = options.email
        this.password = options.password
    }

    public get getSocialTitle(): SocialTitle {
        return this.socialTitle
    }

    public get getFirstname(): string {
        return this.firstname
    }

    public get getLastname(): string {
        return this.lastname
    }

    public get getEmail(): string {
        return this.email
    }

    public get getPassword(): string {
        return this.password
    }

    public toString(): string {
        return `Customer details:
        Socialtitle: ${this.socialTitle.toString()},
        Firstname: ${this.firstname},
        Lastname: ${this.lastname},
        Email: ${this.email},
        Password: ${this.password}
        `
    }
}

export enum SocialTitle {
    MRS, MR
}