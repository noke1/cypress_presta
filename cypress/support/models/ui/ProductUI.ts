interface IProductUI {
    id: number
    idProductAttribute: number
    idAddressDelivery?: number
    quantity: number
}
/*
* Product data compromised from the fields visible in the UI layer
 */
export class ProductUI {

    private id: number
    private idProductAttribute: number
    private idAddressDelivery?: number
    private quantity: number

    constructor(options: IProductUI) {
        this.id = options.id
        this.idProductAttribute = options.idProductAttribute
        this.quantity = options.quantity
        this.idAddressDelivery = options.idAddressDelivery
    }

    public get getId(): number {
        return this.id
    }

    public get getIdProductAttribute(): number {
        return this.idProductAttribute
    }

    public get getIdAddressDelivery(): number {
        return this.idAddressDelivery
    }

    public get getQuantity(): number {
        return this.quantity
    }

    public toString(): string {
        return `Product details:
        Id: ${this.getId}
        IdProductAttribute: ${this.getIdProductAttribute}
        quantity: ${this.getQuantity}
        IdAddressDelivery: ${this.idAddressDelivery}
        `
    }

}
