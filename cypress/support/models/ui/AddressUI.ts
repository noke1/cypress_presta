interface IAddressUI {
    alias?: string
    company?: string
    VATnumber?: string
    addressField: string
    addressComplement?: string
    zipCode: string
    city: string
    phone?: string
}

/**
 * Address compromised from the fields visible on the UI layer
 */
export class AddressUI {
    private alias?: string
    private company?: string
    private VATnumber?: string
    private addressField: string
    private addressComplement?: string
    private zipCode: string
    private city: string
    private phone?: string

    constructor(options: IAddressUI) {
        this.addressField = options.addressField
        this.zipCode = options.zipCode
        this.city = options.city
        this.alias = options.alias
        this.company = options.company
        this.VATnumber = options.VATnumber
        this.addressComplement = options.addressComplement
        this.phone = options.phone
    }

    public get getAlias(): string {
        return this.alias
    }

    public get getCompany(): string {
        return this.company
    }

    public get getVATnumber(): string {
        return this.VATnumber
    }

    public get getAddressField(): string {
        return this.addressField
    }

    public get getAddressComplement(): string {
        return this.addressComplement
    }

    public get getZipCode(): string {
        return this.zipCode
    }

    public get getCity(): string {
        return this.city
    }

    public get getPhone(): string {
        return this.phone
    }

    public toString(): string {
        return `Address details: 
        Alias: ${this.alias},
        Company: ${this.company},
        VATnumber: ${this.VATnumber},
        Address 1: ${this.addressField},
        Address 2: ${this.addressComplement},
        ZipCode: ${this.zipCode},
        City: ${this.city},
        Phone: ${this.phone},
        `
    }
}
