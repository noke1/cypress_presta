import { Group } from "./Group"

interface IGroups {
    groups: Group[]
}

export class Groups {
    group: Group[]

    constructor (data: IGroups) {
        this.group = data.groups
    }
}