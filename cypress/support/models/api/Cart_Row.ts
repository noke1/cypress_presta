interface ICart_Row {
    id_product: number
    id_product_attribute: number
    id_address_delivery?: number
    quantity: number
}

export class Cart_Row {
    id_product: number
    id_product_attribute: number
    id_address_delivery?: number
    quantity: number
    
    constructor (data: ICart_Row) {
        this.id_product = data.id_product
        this.id_product_attribute = data.id_product_attribute
        this.id_address_delivery = data.id_address_delivery
        this.quantity = data.quantity
    }

}