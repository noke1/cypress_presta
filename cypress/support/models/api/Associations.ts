import { Cart_Rows } from './Cart_Rows';
import { Groups } from "./Groups"

interface IAssociations {
    groups?: Groups
    cart_rows?: Cart_Rows
}

export class Associations {
    groups?: Groups
    cart_rows?: Cart_Rows

    constructor(data: IAssociations) {
        this.groups = data.groups
        this.cart_rows = data.cart_rows
    }
}

