interface ILanguage {
    id: string
    name: string
    iso_code: string
    locale: string
    language_code: string
    active: string
    is_rtl: string
    date_format_lite: string
    date_format_full: string
}
export class Language {
    id: string
    name: string
    iso_code: string
    locale: string
    language_code: string
    active: string
    is_rtl: string
    date_format_lite: string
    date_format_full: string

    constructor(data: ILanguage) {
        this.id = data.id,
        this.name = data.name,
        this.iso_code = data.iso_code,
        this.locale = data.locale,
        this.language_code = data.language_code,
        this.active = data.active,
        this.is_rtl = data.is_rtl,
        this.date_format_lite = data.date_format_lite,
        this.date_format_full = data.date_format_full
    }
}