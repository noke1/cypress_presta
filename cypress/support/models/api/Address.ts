interface IAddress {
    id?: string
	id_customer: number
	id_manufacturer?: string
	id_supplier?: string 
	id_warehouse?: string
	id_country: number
	id_state: number
	alias:string 
	company:string 
	lastname: string
	firstname: string
	vat_number: string
	address1: string
	address2: string
	postcode: string
	city: string
	other?: string
	phone: string
	phone_mobile?: string
	dni?: string
	deleted: number
	date_add?: string
	date_upd?: string
}

export class Address {
    id: string = ''
	id_customer: number
	id_manufacturer: string = ''
	id_supplier: string = ''
	id_warehouse: string = ''
	id_country: number
	id_state: number
	alias:string 
	company:string 
	lastname: string
	firstname: string
	vat_number: string
	address1: string
	address2: string
	postcode: string
	city: string
	other: string = ''
	phone: string
	phone_mobile: string = ''
	dni: string = ''
	deleted: number
	date_add: string = ''
	date_upd: string = ''

    constructor(data: IAddress) {
        this.id = data.id
	    this.id_customer = data.id_customer
	    this.id_manufacturer = data.id_manufacturer
	    this.id_supplier = data.id_supplier
	    this.id_warehouse = data.id_warehouse
	    this.id_country = data.id_country
	    this.id_state = data.id_state
	    this.alias = data.alias
	    this.company = data.company
	    this.lastname = data.lastname
	    this.firstname = data.firstname
	    this.vat_number = data.vat_number
	    this.address1 = data.address1
	    this.address2 = data.address2
	    this.postcode = data.postcode
	    this.city = data.city
	    this.other = data.other
	    this.phone = data.phone
	    this.phone_mobile = data.phone_mobile
	    this.dni = data.dni
	    this.deleted = data.deleted
	    this.date_add = data.date_add
	    this.date_upd = data.date_upd
    }

}