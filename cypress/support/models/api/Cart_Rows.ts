import { Cart_Row } from "./Cart_Row"

interface ICart_Rows {
    cart_rows: Cart_Row[]
}

export class Cart_Rows {
    cart_row: Cart_Row[]

    constructor (data: ICart_Rows) {
        this.cart_row = data.cart_rows
    }
}