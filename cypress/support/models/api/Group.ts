import {Language} from './Language'

interface IGroup {
    id?: number
    reduction?:  string
    price_display_method?: string
    show_prices?: string
    date_add?: string
    date_upd?: string
    name?: Language[]
}
export class Group {
    id?: number
    reduction?: string
    price_display_method?: string
    show_prices?: string
    date_add?: string
    date_upd?: string
    name?: Language[]

    constructor (data: IGroup) {
        this.id = data.id,
        this.reduction = data.reduction,
        this.price_display_method = data.price_display_method,
        this.show_prices = data.show_prices,
        this.date_add = data.date_add,
        this.date_upd = data.date_upd,
        this.name = data.name
    }
}