import { Associations } from "./Associations"

interface ICart {
    id: number
    id_currency: number
    id_customer: number
    id_lang: number
    id_shop_group: number
    id_shop: number
    associations: Associations
}


export class Cart {
    id: number
    id_currency: number
    id_customer: number
    id_lang: number
    id_shop_group: number
    id_shop: number
    associations: Associations

    constructor (data: ICart) {
        this.id = data.id
        this.id_currency = data.id_currency
        this.id_customer = data.id_customer
        this.id_lang = data.id_lang
        this.id_shop_group = data.id_shop_group
        this.id_shop = data.id_shop
        this.associations = data.associations
    }
}