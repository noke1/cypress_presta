import { Associations } from "./Associations"

interface ICustomer {
    id?: string
    id_default_group: number
    id_lang?: string
    newsletter_date_add?: string
    ip_registration_newsletter?: string
    last_passwd_gen?: string
    secure_key?: string
    deleted?: string
    passwd: string
    lastname: string
    firstname: string
    email: string
    id_gender: number
    birthday?: string
    newsletter?: string
    optin?: string
    website?: string
    company?: string
    siret?: string
    ape?: string
    outstanding_allow_amount?: string
    show_public_prices?: string
    id_risk?: string
    max_payment_days?: string
    active: number
    note?: string
    is_guest: number
    id_shop: number
    id_shop_group: number
    date_add?: string
    date_upd?: string
    reset_password_token?: string
    reset_password_validity?: string
    associations: Associations
}

export class Customer {
    id: string = ''
    id_default_group: number = 3
    id_lang: string = ''
    newsletter_date_add: string = ''
    ip_registration_newsletter: string = ''
    last_passwd_gen: string = ''
    secure_key: string = ''
    deleted: string = ''
    passwd: string
    lastname: string
    firstname: string
    email: string
    id_gender: number
    birthday: string = ''
    newsletter: string = ''
    optin: string = ''
    website: string = ''
    company: string = ''
    siret: string = ''
    ape: string = ''
    outstanding_allow_amount: string = ''
    show_public_prices: string = ''
    id_risk: string = ''
    max_payment_days: string = ''
    active: number
    note: string = ''
    is_guest: number
    id_shop: number
    id_shop_group: number
    date_add: string = ''
    date_upd: string = ''
    reset_password_token: string = ''
    reset_password_validity: string = ''
    associations: Associations

    constructor (data: ICustomer) {
            this.id = data.id
            this.id_default_group = data.id_default_group
            this.id_lang = data.id_lang
            this.newsletter_date_add = data.newsletter_date_add
            this.ip_registration_newsletter = data.ip_registration_newsletter
            this.last_passwd_gen = data.last_passwd_gen
            this.secure_key = data.secure_key
            this.deleted = data.deleted
            this.passwd = data.passwd
            this.lastname = data.lastname
            this.firstname = data.firstname
            this.email = data.email
            this.id_gender = data.id_gender
            this.birthday = data.birthday
            this.newsletter = data.newsletter
            this.optin = data.optin
            this.website = data.website
            this.company = data.company
            this.siret = data.siret
            this.ape = data.ape
            this.outstanding_allow_amount = data.outstanding_allow_amount
            this.show_public_prices = data.show_public_prices
            this.id_risk = data.id_risk
            this.max_payment_days = data.max_payment_days
            this.active = data.active
            this.note = data.note
            this.is_guest = data.is_guest
            this.id_shop = data.id_shop
            this.id_shop_group = data.id_shop_group
            this.date_add = data.date_add
            this.date_upd = data.date_upd
            this.reset_password_token = data.reset_password_token
            this.reset_password_validity = data.reset_password_validity
            this.associations = data.associations
        }
}