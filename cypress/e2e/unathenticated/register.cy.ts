import navbarPage from '../../pages/NavbarPage'
import signInPage from '../../pages/SignInPage'
import registerPage from '../../pages/RegisterPage'

import { getRandomCustomer } from '../../support/providers/CustomerProvider'

import { Customer } from '../../support/models/api/Customer'

let randomCustomer: Customer

describe('New User Registration', () => {

  beforeEach(() => {
    randomCustomer = getRandomCustomer()

    cy.visit('/')
  })

  afterEach(() => {
    cy.clearCookies()
    cy.reload()
  })

  it('Can register with minimal required data', () => {
    navbarPage.goToSignIn()

    signInPage.goToRegisterPage()

    registerPage.selectGender(randomCustomer)
    registerPage.typePersonalDetails(randomCustomer)
    registerPage.typeCredentials(randomCustomer)
    registerPage.acceptTermsAndConditions()
    registerPage.saveUserDetails()

    navbarPage.getFullNameSpanText()
      .should('equal', `${randomCustomer.firstname} ${randomCustomer.lastname}`)
  })
})