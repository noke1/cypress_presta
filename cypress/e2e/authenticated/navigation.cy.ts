import navbarPage from '../../pages/NavbarPage'
import leftSideBarCategory from "../../pages/category/LeftSideBarCategoryPage"
import mainSectionCategory from "../../pages/category/MainSectionCategoryPage"

import { getRandomCustomer } from '../../support/providers/CustomerProvider'

import { Customer } from '../../support/models/api/Customer'

let randomCustomer: Customer

describe('Check if a logged in user lands in the correct category', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    afterEach(() => {
        cy.clearCookies()
        cy.reload()
    })

    it('Logged user can navigate to Clothes', () => {
        navbarPage.goToClothes()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Clothes')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Clothes'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Clothes')
        cy.url().should('include', 'clothes')
    })

    it('Logged user can navigate to Accessories', () => {
        navbarPage.goToAccessories()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Accessories')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Accessories'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Accessories')
        cy.url().should('include', 'accessories')
    })

    it('Logged user can navigate to Art', () => {
        navbarPage.goToArt()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Art')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Art'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Art')
        cy.url().should('include', 'art')
    })

    it('Logged user can navigate to Clothes > Men subcategory', () => {
        navbarPage.goToClothes()
        leftSideBarCategory.goToMenSubcategory()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Men')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Clothes', 'Men'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Men')
        cy.url().should('include', 'men')
    })

    it('Logged user can navigate to Clothes > Women subcategory', () => {
        navbarPage.goToClothes()
        leftSideBarCategory.goToWomenSubcategory()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Women')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Clothes', 'Women'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Women')
        cy.url().should('include', 'women')
    })

    it('Logged user can navigate to Accessories > Stationery subcategory', () => {
        navbarPage.goToAccessories()
        leftSideBarCategory.goToStationerySubcategory()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Stationery')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Accessories', 'Stationery'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Stationery')
        cy.url().should('include', 'stationery')
    })

    it('Logged user can navigate to Accessories > Home Accessories subcategory', () => {
        navbarPage.goToAccessories()
        leftSideBarCategory.goToHomeAccessoriesSubcategory()

        leftSideBarCategory.getSubcategoriesTitle().should('equal', 'Home Accessories')
        leftSideBarCategory.getBreadcrumbs().then($breadcrumbs => {
            const breadcrumbsText = Cypress._.map($breadcrumbs, 'innerText')
            expect(breadcrumbsText).to.deep.equal(['Home', 'Accessories', 'Home Accessories'])
        })
        mainSectionCategory.getCategoryDescriptionTitle().should('equal', 'Home Accessories')
        cy.url().should('include', 'home-accessories')
    })
})