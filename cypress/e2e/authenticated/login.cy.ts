import navbarPage from '../../pages/NavbarPage'
import signInPage from '../../pages/SignInPage'
import forgotPasswordPage from '../../pages/ForgotPasswordPage'

import { getRandomCustomer } from '../../support/providers/CustomerProvider'

import { Customer } from '../../support/models/api/Customer'

let randomCustomer: Customer

describe('Login in tests', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.registerCustomerViaApiCall(randomCustomer)
        cy.visit('/')
    })

    afterEach(() => {
        cy.clearCookies()
        cy.reload()
    })

    it('Can login with correct credentials', () => {
        navbarPage.goToSignIn()

        signInPage.loginAs(randomCustomer)

        navbarPage.getFullNameSpanText()
            .should('equal', `${randomCustomer.firstname} ${randomCustomer.lastname}`)
    })

})

describe('Login failed tests', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()
        cy.visit('/')
    })

    afterEach(() => {
        cy.clearCookies()
        cy.reload()
    })

    it('Authentication error appears when non-existing user tries to log in', () => {
        navbarPage.goToSignIn()

        signInPage.loginAs(randomCustomer)

        signInPage.getAuthenticationFailedAlert()
            .should('be.visible')
            .and('have.text', 'Authentication failed.')
    })
})


describe('Forgot password', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()
        cy.visit('/')
    })

    afterEach(() => {
        cy.clearCookies()
        cy.reload()
    })

    it('Confirmation message is present when the email is passed to forgot password', () => {
        navbarPage.goToSignIn()
        signInPage.goToForgotPasswordPage()

        forgotPasswordPage.insertEmail(randomCustomer)
        forgotPasswordPage.confirmEmail()

        forgotPasswordPage.getConfirmationSection().find('ul').should('have.class', 'ps-alert-success')
        forgotPasswordPage.getConfirmationSection().find('p').should('include.text', `${randomCustomer.email}`)

    })
})