import navbarPage from '../../../pages/NavbarPage'
import userDetailsDashboardPage from '../../../pages/userdetails/UserDetailsDashboardPage'
import newAddressPage from '../../../pages/userdetails/address/NewAddressPage'
import yourAddressesPage from '../../../pages/userdetails/address/YourAddressesPage'

import { getRandomCustomer } from '../../../support/providers/CustomerProvider'
import { getRandomAddressFullData } from '../../../support/providers/AddressProvider'

import { Customer } from '../../../support/models/api/Customer'
import { Address } from '../../../support/models/api/Address'
import { AddressUI } from '../../../support/models/ui/AddressUI' 

let randomCustomer: Customer
let randomAddress: Address

describe('Add new address', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()
        randomAddress = getRandomAddressFullData()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    it('Can add a first address with minium required data', () => {
        navbarPage.goToPersonalDetails()

        userDetailsDashboardPage.goToAddFirstAddress()
        newAddressPage.fillFormWithMinimumData(randomAddress)
        newAddressPage.saveForm()

        yourAddressesPage.getAddressSuccessAlert()
            .should('be.visible')
            .and('have.text', 'Address successfully added!')
        yourAddressesPage.checkAddressDataCorrectness([
            new AddressUI({
                addressField: randomAddress.address1,
                zipCode: randomAddress.postcode,
                city: randomAddress.city,
                alias: 'My Address'
            })
        ])
    })


    it('Can add a second address with full required data', () => {
        cy.addMinimalAddressToUserViaApiCall(randomCustomer, randomAddress)
        navbarPage.goToPersonalDetails()

        userDetailsDashboardPage.goToAddNextAddress()
        yourAddressesPage.goToCreateNewAddress()
        newAddressPage.fillFormWithFullData(randomAddress)
        newAddressPage.saveForm()

        yourAddressesPage.getAddressSuccessAlert()
            .should('be.visible')
            .and('have.text', 'Address successfully added!')
        yourAddressesPage.checkAddressDataCorrectness([
            new AddressUI({
                addressField: randomAddress.address1,
                zipCode: randomAddress.postcode,
                city: randomAddress.city,
                alias: randomAddress.alias
            }), 
            new AddressUI({
                addressField: randomAddress.address1,
                addressComplement: randomAddress.address2,
                zipCode: randomAddress.postcode,
                city: randomAddress.city,
                alias: randomAddress.alias,
                company: randomAddress.company,
                VATnumber: randomAddress.vat_number,
                phone: randomAddress.phone
            })
        ])
    })
})


describe('Delete address', () => {
    beforeEach(() => {
        randomCustomer = getRandomCustomer()
        randomAddress = getRandomAddressFullData()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })
    
    it('Can delete 2 addresses successfully - success message is present after each deletion, addresses are gone', () => {
        cy.addFullAddressToUserViaApiCall(randomCustomer, randomAddress)
        cy.addFullAddressToUserViaApiCall(randomCustomer, randomAddress)
        navbarPage.goToPersonalDetails()
        userDetailsDashboardPage.goToAddNextAddress()

        yourAddressesPage.deleteAllAddresses()

        yourAddressesPage.getAddressSuccessAlert()
            .should('be.visible')
            .and('have.text', 'Address successfully deleted!')
        yourAddressesPage.getAddressesArticles().should('not.exist')
    })
})

describe('Edit address', () => {
    beforeEach(() => {
        randomCustomer = getRandomCustomer()
        randomAddress = getRandomAddressFullData()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.addFullAddressToUserViaApiCall(randomCustomer, randomAddress)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    it('Can edit the address successfully - success message is present, address changed', () => {
        const newRandomAddress: Address = getRandomAddressFullData()
        navbarPage.goToPersonalDetails()
        userDetailsDashboardPage.goToAddNextAddress()

        yourAddressesPage.editAddress()
        newAddressPage.fillFormWithFullData(newRandomAddress)
        newAddressPage.saveForm()

        yourAddressesPage.getAddressSuccessAlert()
            .should('be.visible')
            .and('have.text', 'Address successfully updated!')
        yourAddressesPage.checkAddressDataCorrectness([
            new AddressUI({
                addressField: newRandomAddress.address1,
                addressComplement: newRandomAddress.address2,
                zipCode: newRandomAddress.postcode,
                city: newRandomAddress.city,
                alias: newRandomAddress.alias,
                company: newRandomAddress.company,
                VATnumber: newRandomAddress.vat_number,
                phone: newRandomAddress.phone
            })
        ])
    })
})