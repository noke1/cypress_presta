import navbarPage from '../../../pages/NavbarPage'
import userDetailsDashboardPage from '../../../pages/userdetails/UserDetailsDashboardPage'
import gdprPersonalDataPage from "../../../pages/userdetails/GDPRPersonalDataPage"

import { getRandomCustomer } from '../../../support/providers/CustomerProvider'

import { Customer } from "../../../support/models/api/Customer"

let randomCustomer: Customer

describe('Download a GDPR file into a local machine', () => {
    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    it('Can download a GDPR PDF file into the local machine and the file is not empty', () => {
        const todaysDate = new Date().toJSON().slice(0, 10)
        const fileName = `personalData-${todaysDate}.pdf`
        const filePath = `cypress\\downloads\\${fileName}`

        navbarPage.goToPersonalDetails()
        userDetailsDashboardPage.goToGDPRPersonalData()

        gdprPersonalDataPage.clickGetToPDF()
        cy.readFile(filePath).should('exist').and('not.be.empty')
    })

    it('Can download a GDPR CSV file into the local machine and the file is not empty', () => {
        const todaysDate = new Date().toJSON().slice(0, 10)
        const fileName = `personalData-${todaysDate}.csv`
        const filePath = `cypress\\downloads\\${fileName}`

        navbarPage.goToPersonalDetails()
        userDetailsDashboardPage.goToGDPRPersonalData()

        gdprPersonalDataPage.clickGetToCSV()
        cy.readFile(filePath).should('exist').and('not.be.empty')
    })
})