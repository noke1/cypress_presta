import { getRandomCustomer } from '../../../support/providers/CustomerProvider'
import navbarPage from '../../../pages/NavbarPage'
import registerPage from '../../../pages/RegisterPage'
import userDetailsDashboardPage from '../../../pages/userdetails/UserDetailsDashboardPage'

import { Customer } from "../../../support/models/api/Customer"

let randomCustomer: Customer


describe('Personal information edit', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
        navbarPage.goToPersonalDetails()
    })

    it('Can change password - confirmation alert appears', () => {
        userDetailsDashboardPage.goToInformation()

        registerPage.typePassword(randomCustomer)
        registerPage.typeNewPassword(randomCustomer)
        registerPage.acceptTermsAndConditions()
        registerPage.saveUserDetails()

        registerPage.getInformationUpdateSuccessInfo()
            .first()
            .should('be.visible')
            .invoke('text')
            .and('eq', 'Information successfully updated.')
    })

    it('Can sign to receive offers from partners - confirmation alert appears', () => {
        userDetailsDashboardPage.goToInformation()

        registerPage.typePassword(randomCustomer)
        registerPage.typeNewPassword(randomCustomer)
        registerPage.receiveOffersFromPartners()
        registerPage.acceptTermsAndConditions()
        registerPage.saveUserDetails()

        registerPage.getInformationUpdateSuccessInfo()
            .first()
            .should('be.visible')
            .invoke('text')
            .and('eq', 'Information successfully updated.')

    })

    it('Can sign to newsletter - confirmation alert appears', () => {
        userDetailsDashboardPage.goToInformation()

        registerPage.typePassword(randomCustomer)
        registerPage.typeNewPassword(randomCustomer)
        registerPage.signUpForNewsletter().should('be.checked')
        registerPage.acceptTermsAndConditions()
        registerPage.saveUserDetails()

        registerPage.getInformationUpdateSuccessInfo()
            .first()
            .should('be.visible')
            .invoke('text')
            .and('eq', 'Information successfully updated.')
    })

    it('Can resign from newsletter - confirmation alert appears', () => {
        userDetailsDashboardPage.goToInformation()

        registerPage.typePassword(randomCustomer)
        registerPage.typeNewPassword(randomCustomer)
        registerPage.signUpForNewsletter().should('be.checked')
        registerPage.signUpForNewsletter().should('not.be.checked')
        registerPage.acceptTermsAndConditions()
        registerPage.saveUserDetails()

        registerPage.getInformationUpdateSuccessInfo()
            .first()
            .should('be.visible')
            .invoke('text')
            .and('eq', 'Information successfully updated.')
    })

})