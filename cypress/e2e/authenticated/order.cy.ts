import { formatOrderConfirmation } from "../../support/utils/extract"

import { getRandomCustomer } from "../../providers/CustomerProvider"
import { getRandomAddressFullData } from "../../support/providers/AddressProvider"

import shoppingCartPage from "../../pages/ShoppingCartPage"
import orderPage, { PaymentMethod, ShippingMethod } from "../../pages/order/OrderPage"
import orderSummaryPage from "../../pages/order/OrderSummaryPage"

import { Product } from "../../providers/models/Product"
import { Customer } from "../../support/api/models/Customer"
import { Address } from "../../support/api/models/Address"

let randomCustomer: Customer
let randomFullAddress: Address


describe('Order preparing', () => {
    beforeEach(() => {
        randomCustomer = getRandomCustomer()
        randomFullAddress = getRandomAddressFullData()
        const product: Product = new Product({ id: 3, idProductAttribute: 13, quantity: 4 })

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.createCustomerCartWithProduct(product, randomCustomer)
        cy.visit('en/cart?action=show')
    })

    interface OrderDetails {
        shippingMethod: ShippingMethod,
        shippingComment: string,
        paymentMethod: PaymentMethod
    }

    const details: OrderDetails[] = [
        { shippingMethod: 'test shoop', shippingComment: 'test', paymentMethod: 'check' },
        { shippingMethod: 'My carrier', shippingComment: 'test', paymentMethod: 'check' },
        { shippingMethod: 'test shoop', shippingComment: 'test', paymentMethod: 'bank wire' },
        { shippingMethod: 'My carrier', shippingComment: 'test', paymentMethod: 'bank wire' },
    ]

    details.forEach(orderDetail => {
        it(`Can complete the order with carrier: ${orderDetail.shippingMethod} and payment: ${orderDetail.paymentMethod} then redirected to the order summary page`, () => {
            shoppingCartPage.proceedToCheckout()

            orderPage.fillFormWithMinimumData(randomFullAddress)
            orderPage.confirmAddress()
            orderPage.pickShippingMethod(orderDetail.shippingMethod)
            orderPage.provideShippingComment(orderDetail.shippingComment)
            orderPage.confirmShipping()
            orderPage.pickPaymentMethod(orderDetail.paymentMethod)
            orderPage.acceptTermsAndConditions()
            orderPage.confirmOrder()

            cy.location().its('pathname').should('eq', '/en/order-confirmation')
            orderSummaryPage.getOrderConfirmed()
                .should('be.visible')
                .invoke('text')
                .then(formatOrderConfirmation)
                .should('eq', 'Your order is confirmed')
        })
    })

    it('Checks the data passed during order placement against the order summary', () => {
        shoppingCartPage.proceedToCheckout()

        orderPage.fillFormWithMinimumData(randomFullAddress)
        orderPage.confirmAddress()
        orderPage.pickShippingMethod('test shoop')
        orderPage.provideShippingComment('test')
        orderPage.confirmShipping()
        orderPage.pickPaymentMethod('bank wire')
        orderPage.acceptTermsAndConditions()
        orderPage.confirmOrder()

        orderSummaryPage.getEmailSent()
            .invoke('text')
            .invoke('replace', /\n/g, '')
            .invoke('replace', /\s{2,}/g, '')
            .should('eq', `An email has been sent to your mail address ${randomCustomer.email}.`)
        orderSummaryPage.getOrderDetailsInfo().then($li => {
            const text: string[] = Cypress.$.makeArray($li).map(el => el.innerText)
            expect(text[0]).to.match(/^Order reference: [A-Z]{9}$/)
            expect(text[1]).to.eq('Payment method: Bank transfer')
            expect(text[2]).to.eq('Shipping method: test shoop\nPick up in-store')
        })
    })
})
