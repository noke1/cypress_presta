import navbarPage from '../../pages/NavbarPage'
import leftSideBarCategory from '../../pages/category/LeftSideBarCategoryPage'
import mainSectionCategory from "../../pages/category/MainSectionCategoryPage"

import { getRandomCustomer } from '../../support/providers/CustomerProvider'

import { Customer } from "../../support/models/api/Customer"

let randomCustomer: Customer

describe('Filtering the products', () => {
    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    afterEach(() => {
        cy.clearCookies()
        cy.reload()
    })

    interface FilterDetails {
        categoryName: string,
        interceptUrl: string,
        expectedPhrase: string,
    }

    const filters: FilterDetails[] = [
        { categoryName: 'Stationery', interceptUrl: '/en/6-accessories?q=Categories-Stationery&from-xhr', expectedPhrase: 'stationery' },
        { categoryName: 'Home Accessories', interceptUrl: '/en/6-accessories?q=Categories-Home+Accessories&from-xhr', expectedPhrase: 'home' },
    ]

    filters.forEach(filter => {
        it(`Filters the Accessories products by ${filter.categoryName} category`, () => {
            cy.intercept('GET', `${filter.interceptUrl}`).as('filterCall')

            navbarPage.goToAccessories()
            leftSideBarCategory.selectFilterByCategory(`${filter.categoryName}`)

            cy.wait('@filterCall').should(({ response }) => {
                const { products } = response.body

                products.map(({ canonical_url }: { canonical_url: string }) => {
                    expect(canonical_url).to.contain(`${filter.expectedPhrase}`)
                })
            })
        })
    })
})

describe('Sorting the products', () => {
    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    afterEach(() => {
        cy.clearCookies()
        cy.reload()
    })

    it('Sorts the Art products by Name A to Z (asc)', () => {
        cy.intercept('GET', '/en/9-art?order=product.name.asc&from-xhr').as('getArtProductsSortedByNameAsc')

        navbarPage.goToArt()
        mainSectionCategory.sortBy('Name, A to Z')
        cy.wait('@getArtProductsSortedByNameAsc')

        mainSectionCategory.getProductNames().should($productNames => {
            const names: string[] = Cypress._.map($productNames, 'innerText')
            const sorted: string[] = Cypress._.sortBy(names)
            expect(sorted).to.deep.equal(names)
        })
    })

    it('Sorts the Art products by Name Z to A (desc)', () => {
        cy.intercept('GET', '/en/9-art?order=product.name.desc&from-xhr').as('getArtProductsSortedByNameDesc')

        navbarPage.goToArt()
        mainSectionCategory.sortBy('Name, Z to A')
        cy.wait('@getArtProductsSortedByNameDesc')

        mainSectionCategory.getProductNames().should($productNames => {
            const names: string[] = Cypress._.map($productNames, 'innerText')
            const sorted: string[] = Cypress._.sortBy(names).reverse()
            expect(sorted).to.deep.equal(names)
        })
    })

    it('Sorts the Art products by Price low to high (asc)', () => {
        cy.intercept('GET', '/en/9-art?order=product.price.asc&from-xhr').as('getArtProductsSortedByPriceAsc')

        navbarPage.goToArt()
        mainSectionCategory.sortBy('Price, low to high')
        cy.wait('@getArtProductsSortedByPriceAsc')

        mainSectionCategory.getProductPrices().should($productPrices => {
            const prices: number[] = Cypress._.map($productPrices, extractPrice)
            const sorted: number[] = Cypress._.sortBy(prices)
            expect(sorted).to.deep.equal(prices)
        })
    })

    it('Sorts the Art products by Price high to low (desc)', () => {
        cy.intercept('GET', '/en/9-art?order=product.price.desc&from-xhr').as('getArtProductsSortedByPriceDesc')

        navbarPage.goToArt()
        mainSectionCategory.sortBy('Price, high to low')
        cy.wait('@getArtProductsSortedByPriceDesc')

        mainSectionCategory.getProductPrices().should($productPrices => {
            const prices: number[] = Cypress._.map($productPrices, extractPrice)
            const sorted: number[] = Cypress._.sortBy(prices).reverse()
            expect(sorted).to.deep.equal(prices)
        })
    })

    function extractPrice(element: HTMLElement): number {
        let priceWithCurrency: string = element.innerText
        return +priceWithCurrency.replace(/[^0-9.]/g, '')
    }
})