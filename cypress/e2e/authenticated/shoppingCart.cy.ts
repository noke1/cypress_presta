import navbarPage from '../../pages/NavbarPage'
import mainSectionCategory from "../../pages/category/MainSectionCategoryPage"
import productPage from "../../pages/ProductPage"
import shoppingCartPage from "../../pages/ShoppingCartPage"

import { getRandomCustomer } from '../../support/providers/CustomerProvider'

import { Customer } from '../../support/models/api/Customer'

let randomCustomer: Customer

describe('Shopping cart tests', () => {

    beforeEach(() => {
        randomCustomer = getRandomCustomer()

        cy.clearCookies()
        cy.reload()
        cy.registerCustomerViaApiCall(randomCustomer)
        cy.loginViaApiCall(randomCustomer)
        cy.visit('/')
    })

    it('Can add random ART product with X quantity to Cart', () => {
        const quantity: number = 3
        navbarPage.goToArt()
        mainSectionCategory.pickRandomProduct()
        productPage.selectQuantity(quantity)
        productPage.addProductToCart()
        productPage.proceedToCheckout()

        navbarPage.getShoppingCartItemsCount().should('equal', quantity)
    })

    it('Removes the product from the cart, so the cart is empty', () => {
        cy.createCustomerCartWithProduct(randomCustomer)
        cy.visit('en/cart?action=show')
        shoppingCartPage.getSingleQuantityPrice().should('be.above', 0)

        shoppingCartPage.deleteProductsFromCart()

        shoppingCartPage.getMessageCartEmptyText()
            .should('not.be.empty')
            .and('eq', 'There are no more items in your cart')
    })

    it('Calculates the total price of added product correctly', () => {
        const quantity: number = 3
        navbarPage.goToArt()
        mainSectionCategory.pickRandomProduct()
        productPage.selectQuantity(quantity)
        productPage.addProductToCart()
        productPage.proceedToCheckout()

        shoppingCartPage.getTotalPrice().as('totalPrice')
        shoppingCartPage.getQuantitiesForProduct().as('quantity')
        shoppingCartPage.getSingleQuantityPrice().as('singleProductPrice')

        cy.then(function () {
            const calculatedTotalPrice = +(this.singleProductPrice * this.quantity).toFixed(2)
            expect(this.totalPrice).to.be.equal(calculatedTotalPrice)
        })
    })
})