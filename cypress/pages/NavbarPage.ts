class NavbarPage {

    elements = {
        signInButton: () => cy.contains('Sign in'),
        signOutButton: () => cy.get('#_desktop_user_info a').contains('Sign out'),
        fullNameSpan: () => cy.get('#_desktop_user_info span'),
        clothesAnchor: () => cy.get('#category-3 > a'),
        accessoriesAnchor: () => cy.get('#category-6 > a'),
        artAnchor: () => cy.get('#category-9 > a'),
        shoppingCartItemsCountSpan: () => cy.get('.cart-products-count'),
    }

    goToSignIn(): void {
        this.elements.signInButton().click()
    }

    signOut(): void {
        this.elements.signOutButton().click()
    }

    goToClothes(): void {
        this.elements.clothesAnchor().click()
    }

    goToAccessories(): void {
        this.elements.accessoriesAnchor().click()
    }

    goToArt(): void {
        this.elements.artAnchor().click()
    }

    goToPersonalDetails(): void {
        this.elements.fullNameSpan().click()
    }

    getShoppingCartItemsCount() {
        return this.elements.shoppingCartItemsCountSpan()
            .invoke('text')
            .then(this.convertToNumber)
    }

    getFullNameSpanText() {
        return this.elements.fullNameSpan().invoke('text')
    }

    private convertToNumber(text: string): number {
        return +text.trim().replace(/[^0-9.]/g, '')
    }
}

export default new NavbarPage()