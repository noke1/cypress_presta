class GDPRPersonalDataPage {
    elements = {
        getToPDFAnchor: () => cy.get('a#exportDataToPdf'),
        getToCSVAnchor: () => cy.get('a#exportDataToCsv'),
    }

    clickGetToPDF(): void {
        this.elements.getToPDFAnchor().click()
    }

    clickGetToCSV(): void {
        this.elements.getToCSVAnchor().click()
    }
}


export default new GDPRPersonalDataPage()