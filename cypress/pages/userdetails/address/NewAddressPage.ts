import { Address } from "../../../support/models/api/Address"

class NewAddressPage {

    elements = {
        aliasInput: () => cy.get('input[name="alias"]'),
        companyInput: () => cy.get('input[name="company"]'),
        VATnumberInput: () => cy.get('input[name="vat_number"]'),
        addressFieldInput: () => cy.get('input[name="address1"]'),
        addressComplementInput: () => cy.get('input[name="address2"]'),
        zipcodeInput: () => cy.get('input[name="postcode"]'),
        cityInput: () => cy.get('input[name="city"]'),
        phoneInput: () => cy.get('input[name="phone"]'),
        saveFormButton: () => cy.get('footer button[type="submit"]')
    }

    fillFormWithMinimumData(address: Address): void {
        this.elements.addressFieldInput().clear().type(address.address1)
        this.elements.zipcodeInput().clear().type(address.postcode)
        this.elements.cityInput().clear().type(address.city)
    }

    fillFormWithFullData(address: Address): void {
        this.elements.aliasInput().clear().type(address.alias)
        this.elements.companyInput().clear().type(address.company)
        this.elements.VATnumberInput().clear().type(address.vat_number)
        this.elements.addressFieldInput().clear().type(address.address1)
        this.elements.addressComplementInput().clear().type(address.address2)
        this.elements.zipcodeInput().clear().type(address.postcode)
        this.elements.cityInput().clear().type(address.city)
        this.elements.phoneInput().clear().type(address.phone)
    }

    saveForm(): void {
        this.elements.saveFormButton().click()
    }
}

export default new NewAddressPage()