import { AddressUI } from "../../../support/models/ui/AddressUI"


class YourAddressesPage {

    elements = {
        //This is the selector for the success alert. So it is the same for adding and address, deleting an address, editing an address
        addressSuccessAlert: () => cy.get('[data-alert="success"] ul li'),
        addressesArticles: () => cy.get('article[id^="address-"'),
        createNewAddressAnchor: () => cy.get('a[data-link-action="add-address"]'),
    }

    goToCreateNewAddress(): void {
        this.elements.createNewAddressAnchor().click()
    }

    /*
        There is a problem that each article after the deletion disappears. 
        So to get around the element detached from DOM problem I need to re-query the articles list before clicking the delete.
     */
    deleteAllAddresses(): void {
        this.elements.addressesArticles()
            .as('addressArticles')
            .each(() => {
                cy.get('@addressArticles')
                    .find('[data-link-action="delete-address"]')
                    .first()
                    .should('be.visible')
                    .click()
            })
    }

    editAddress(): void {
        this.elements.addressesArticles()
            .find('[data-link-action="edit-address"]')
            .click()
    }

    checkAddressDataCorrectness(givenAddresses: AddressUI[]): void {
        const addressMiniumDataPropertiesCount: number = 4

        this.elements.addressesArticles()
            .each($addressArticle => {
                cy.wrap($addressArticle)
                    .within(() => {
                        cy.get('h4').as('addressAlias')
                    })
                    .find('address')
                    .invoke('html')
                    .then(addressData => {
                        const addressDataFormatted: string[] = addressData.split('<br>')
                        expect(addressDataFormatted).to.have.length.gte(addressMiniumDataPropertiesCount)
                        return addressDataFormatted
                    })
                    .then(addressDataFormatted => {
                        cy.get('@addressAlias')
                            .invoke('text')
                            .then(addressAliasText => {
                                if (addressDataFormatted.length === addressMiniumDataPropertiesCount) {
                                    cy.wrap(this.parseToAddressMinimumData(addressAliasText, addressDataFormatted)).should($addressObj => {
                                        expect(givenAddresses).to.deep.include($addressObj)
                                    })
                                }
                                else {
                                    cy.wrap(this.parseToAddressFullData(addressAliasText, addressDataFormatted)).should($addressObj => {
                                        expect(givenAddresses).to.deep.include($addressObj)
                                    })
                                }
                            })
                    })
            })
    }

    getAddressSuccessAlert() {
        return this.elements.addressSuccessAlert()
    }

    getAddressesArticles() {
        return this.elements.addressesArticles()
    }

    private parseToAddressMinimumData(alias: string, data: string[]): AddressUI {
        const [, addressField, zipCodeCity,] = data
        const [zipCode, city] = zipCodeCity.split(/\s(.*)/)
        
        return new AddressUI({
            addressField,
            zipCode,
            city,
            alias
        })
    }
    
    private parseToAddressFullData(alias: string, data: string[]): AddressUI {
        const [, company, VATnumber, addressField, addressComplement, zipCodeCity, , phone] = data
        const [zipCode, city] = zipCodeCity.split(/\s(.*)/)

        return new AddressUI({
            addressField,
            zipCode,
            city,
            alias,
            company,
            VATnumber,
            addressComplement,
            phone,
        })
    }
}




export default new YourAddressesPage()