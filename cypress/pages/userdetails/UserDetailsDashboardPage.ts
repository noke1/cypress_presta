class UserDetailsDashboardPage {

    elements = {
        informationIcon: () => cy.get('#identity-link span i'),
        addFirstAddressIcon: () => cy.get('#address-link span i'), //This one is available when no addresses yet assigned to the account
        addNextAddressIcon: () => cy.get('#addresses-link span i'), //This one is available when at least one address is already assigned to the account
        orderHistoryAndDetailsIcon: () => cy.get('#history-link span i'),
        creditSlipsIcon: () => cy.get('#order=slips-link span i'),
        gdprersonalDataIcon: () => cy.get('#psgdpr-link span i'),
    }

    goToInformation(): void {
        this.elements.informationIcon().click()
    }

    goToAddFirstAddress(): void {
        this.elements.addFirstAddressIcon().click()
    }

    goToAddNextAddress(): void {
        this.elements.addNextAddressIcon().click()
    }

    goToOrderHistoryAndDetails(): void {
        this.elements.orderHistoryAndDetailsIcon().click()
    }

    goToCreditSlips(): void {
        this.elements.creditSlipsIcon().click()
    }

    goToGDPRPersonalData(): void {
        this.elements.gdprersonalDataIcon().click()
    }
}

export default new UserDetailsDashboardPage()