import { Customer } from "../support/models/api/Customer"

class RegisterPage {

    elements = {
        possibleGendersRadios: () => cy.get('input[name="id_gender"]'),
        firstnameInput: () => cy.get('input[name="firstname"]'),
        lastnameInput: () => cy.get('input[name="lastname"]'),
        emailInput: () => cy.get('#customer-form input[name="email"]'),
        passwordInput: () => cy.get('input[name="password"]'),
        newPasswordInput: () => cy.get('input[name="new_password"]'),
        termsAndConditionsCheckbox: () => cy.get('input[name="psgdpr"]'),
        receiveOffersFromPartnersCheckbox: () => cy.get('input[name="optin"]'),
        signUpForNewsletterCheckbox: () => cy.get('input[name="newsletter"]'),
        registerButton: () => cy.get('[data-link-action="save-customer"]'),
        informationUpdatedSuccessInfo: () => cy.get('#notifications ul li'),
    }

    selectGender(customer: Customer): void {
        const genderValue: number = customer.id_gender

        this.elements.possibleGendersRadios().each(($el) => {
            if (Number($el.attr('value')) === genderValue) {
                cy.wrap($el).click()
            }
        })
    }

    typePersonalDetails(customer: Customer): void {
        this.elements.firstnameInput().type(customer.firstname)
        this.elements.lastnameInput().type(customer.lastname)
    }

    typeCredentials(customer: Customer): void {
        this.elements.emailInput().type(customer.email)
        this.elements.passwordInput().type(customer.passwd)
    }

    typeNewPassword(customer: Customer): void {
        this.elements.newPasswordInput().type(customer.passwd)
    }

    typePassword(customer: Customer): void {
        this.elements.passwordInput().type(customer.passwd)
    }

    acceptTermsAndConditions(): void {
        this.elements.termsAndConditionsCheckbox().click()
    }

    receiveOffersFromPartners(): void {
        this.elements.receiveOffersFromPartnersCheckbox().click().should('be.checked')
    }

    signUpForNewsletter() {
        return this.elements.signUpForNewsletterCheckbox().click()
    }

    saveUserDetails(): void {
        this.elements.registerButton().click()
    }

    getInformationUpdateSuccessInfo() {
        return this.elements.informationUpdatedSuccessInfo()
    }
}

export default new RegisterPage()