import { Customer } from "../support/models/api/Customer"

class ForgotPasswordPage {

    elements = {
        emailInput: () => cy.get('input#email'),
        sendButton: () => cy.get('#content button[type="submit"]').eq(0), //there are actually 2 quite same buttons
        confirmationSection: () => cy.get('#content')
    }

    insertEmail(customer: Customer): void {
        this.elements.emailInput().type(customer.email)
    }

    confirmEmail(): void {
        this.elements.sendButton().click()
    }

    getConfirmationSection() {
        return this.elements.confirmationSection()
    }
}

export default new ForgotPasswordPage()