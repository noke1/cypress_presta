import { Customer } from "../support/models/api/Customer"

class SignInPage {

    elements = {
        goToRegisterAnchor: () => cy.get('a[data-link-action="display-register-form"]'),
        emailInput: () => cy.get('#login-form input[name="email"]'),
        passwordInput: () => cy.get('#login-form input[name="password"]'),
        logInButton: () => cy.get('#submit-login'),
        authenticationFailedAlert: () => cy.get('li.alert.alert-danger'),
        forgotPasswordAnchor: () => cy.get('#login-form div.forgot-password')
    }

    goToRegisterPage(): void {
        this.elements.goToRegisterAnchor().click()
    }

    loginAs(customer: Customer): void {
        this.elements.emailInput().type(customer.email)
        this.elements.passwordInput().type(customer.passwd)
        this.elements.logInButton().click()
    }

    getAuthenticationFailedAlert() {
        return this.elements.authenticationFailedAlert()
    }

    goToForgotPasswordPage() {
        return this.elements.forgotPasswordAnchor().click()
    }
}

export default new SignInPage()