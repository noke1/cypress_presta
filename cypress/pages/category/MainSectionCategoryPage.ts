/// <reference types="cypress-xpath" />
require('cypress-xpath')

class MainSectionCategory {

    elements = {
        categoryDescriptionTitle: () => cy.xpath('//div[@id="category-description"]/preceding-sibling::h1'),
        sortByButton: () => cy.get('#products button').eq(0),
        sortByOptions: () => cy.get('#products .dropdown-menu a'),
        productNames: () => cy.get('#js-product-list article [itemprop="name"] a'),
        productPrices: () => cy.get('#js-product-list span[itemprop="price"]')
    }

    expandSortByOptions(): void {
        this.elements.sortByButton().click()
    }

    sortBy(option: string) {
        this.expandSortByOptions()
        this.elements.sortByOptions().contains(option).click({ force: true })
    }

    getProductNames(): Cypress.Chainable<JQuery<HTMLElement>> {
        return this.elements.productNames()
    }

    getProductPrices(): Cypress.Chainable<JQuery<HTMLElement>> {
        return this.elements.productPrices()
    }

    pickRandomProduct(): void {
        this.elements.productNames().then($products => {
            const position = Cypress._.random(0, $products.length - 1)
            cy.log(`***Picked a random product position of: ${position}***`)
            cy.log(`***Trying to click the product named: ${$products[position].innerText}***`)
            cy.wrap($products[position]).click()
        })
    }

    getCategoryDescriptionTitle(): Cypress.Chainable<string> {
        return this.elements.categoryDescriptionTitle().invoke('text')
    }
}

export default new MainSectionCategory()