class LeftSideBarCategory {

    locators = {
        subcategoriesTitle: '#left-column a.text-uppercase.h6',
        breadcrumbsSpan: 'span[itemprop="name"]',
        menSubcategoryAnchor: '.category-sub-menu a',
        womenSubcategoryAnchor: '.category-sub-menu a',
        stationerySubcategoryAnchor: '.category-sub-menu a',
        homeAccessoriesSubcategoryAnchor: '.category-sub-menu a',
        filterCategoriesAnchor: '[id^="facet_"] li a',
    }

    goToMenSubcategory(): void {
        cy.get(this.locators.menSubcategoryAnchor).eq(0).click()
    }

    goToWomenSubcategory(): void {
        cy.get(this.locators.womenSubcategoryAnchor).eq(1).click()
    }

    goToStationerySubcategory(): void {
        cy.get(this.locators.stationerySubcategoryAnchor).eq(0).click()
    }

    goToHomeAccessoriesSubcategory(): void {
        cy.get(this.locators.homeAccessoriesSubcategoryAnchor).eq(1).click()
    }

    selectFilterByCategory(category: string): void {
        cy.get(this.locators.filterCategoriesAnchor).contains(category).click()
    }

    getBreadcrumbs() {
        return cy.get(this.locators.breadcrumbsSpan)
    }

    getSubcategoriesTitle() {
        return cy.get(this.locators.subcategoriesTitle).invoke('text')
    }

}

export default new LeftSideBarCategory()