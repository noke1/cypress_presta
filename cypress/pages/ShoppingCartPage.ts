class ShoppingCartPage {

    elements = {
        singleQuantityPriceSpan: () => cy.get('.current-price span.price'),
        quantitiesInput: () => cy.get('input[name="product-quantity-spin"]'),
        totalPriceStrong: () => cy.get('.product-price strong'),
        deleteIcons: () => cy.get('[data-link-action="delete-from-cart"] i'),
        messageEmptyCartSpan: () => cy.get('span.no-items'),
        proceedToCheckoutAnchor: () => cy.get('a[href$="/en/cart?action=show&checkout"]'),
    }

    deleteProductsFromCart(): void {
        this.elements.deleteIcons()
            .each($deleteIcon => {
                cy.wrap($deleteIcon).click()
            })
    }

    getTotalPrice() {
        return this.elements.totalPriceStrong()
            .invoke('text')
            .then(this.convertToNumber)
    }

    getQuantitiesForProduct() {
        return this.elements.quantitiesInput()
            .invoke('val')
            .then(this.convertToNumber)
    }

    getSingleQuantityPrice() {
        return this.elements.singleQuantityPriceSpan()
            .invoke('text')
            .then(this.convertToNumber)
    }

    getMessageCartEmptyText() {
        return this.elements.messageEmptyCartSpan().invoke('text')
    }

    proceedToCheckout(): void {
        this.elements.proceedToCheckoutAnchor().click()
    }

    private convertToNumber(text: string): number {
        return +text.trim().replace(/[^0-9.]/g, '')
    }

}

export default new ShoppingCartPage()