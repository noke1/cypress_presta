class OrderSummaryPage {

    elements = {
        orderConfirmedText: () => cy.get('#content-hook_order_confirmation h3'),
        emailSentText: () => cy.get('#content-hook_order_confirmation p'),
        orderDetailsInfo: () => cy.get('#order-details li'),
    }

    getOrderConfirmed() {
        return this.elements.orderConfirmedText()
    }

    getEmailSent() {
        return this.elements.emailSentText()
    }

    getOrderDetailsInfo() {
        return this.elements.orderDetailsInfo()
    }

}

export default new OrderSummaryPage()