import { Address } from "../../support/models/api/Address"

class OrderPage {

    elements = {
        aliasInput: () => cy.get('input[name="alias"]'),
        companyInput: () => cy.get('input[name="company"]'),
        VATnumberInput: () => cy.get('input[name="vat_number"]'),
        addressFieldInput: () => cy.get('input[name="address1"]'),
        addressComplementInput: () => cy.get('input[name="address2"]'),
        zipcodeInput: () => cy.get('input[name="postcode"]'),
        cityInput: () => cy.get('input[name="city"]'),
        phoneInput: () => cy.get('input[name="phone"]'),
        confirmAddressButton: () => cy.get('button[name="confirm-addresses"]'),
        shippingMethodsDiv: () => cy.get('div.delivery-options'),
        shippingMessageTextarea: () => cy.get('#delivery_message'),
        confirmShippingButton: () => cy.get('button[name="confirmDeliveryOption"]'),
        paymentMethodsDiv: () => cy.get('div.payment-options'),
        termsAndConditionsInput: () => cy.get('[name="conditions_to_approve[terms-and-conditions]"]'),
        confirmOrderButton: () => cy.get('#payment-confirmation button'),
    }

    fillFormWithMinimumData(address: Address): void {
        this.elements.addressFieldInput().clear().type(address.address1)
        this.elements.zipcodeInput().clear().type(address.postcode)
        this.elements.cityInput().clear().type(address.city)
    }

    fillFormWithFullData(address: Address): void {
        this.elements.aliasInput().clear().type(address.alias)
        this.elements.companyInput().clear().type(address.company)
        this.elements.VATnumberInput().clear().type(address.vat_number)
        this.elements.addressFieldInput().clear().type(address.address1)
        this.elements.addressComplementInput().clear().type(address.address2)
        this.elements.zipcodeInput().clear().type(address.postcode)
        this.elements.cityInput().clear().type(address.city)
        this.elements.phoneInput().clear().type(address.phone)
    }

    confirmAddress(): void {
        this.elements.confirmAddressButton().click()
    }

    pickShippingMethod(method: ShippingMethod): void {
        this.elements.shippingMethodsDiv().then($div => {
            if (method === 'test shoop') {
                cy.wrap($div).contains('span', 'test shoop').click()
                cy.wrap($div).find('input#delivery_option_1').should('be.checked')
            } else if (method === 'My carrier') {
                cy.wrap($div).contains('span', 'My carrier').click()
                cy.wrap($div).find('input#delivery_option_2').should('be.checked')
            }
        })
    }

    provideShippingComment(comment: string): void {
        this.elements.shippingMessageTextarea().type(comment)
    }

    confirmShipping(): void {
        this.elements.confirmShippingButton().click()
    }

    pickPaymentMethod(method: PaymentMethod): void {
        this.elements.paymentMethodsDiv().then($div => {
            if (method === 'check') {
                cy.wrap($div).contains('span', 'Pay by Check').click()
                cy.wrap($div).find('input[data-module-name="ps_checkpayment"]').should('be.checked')
            } else if (method === 'bank wire') {
                cy.wrap($div).contains('span', 'Pay by bank wire').click()
                cy.wrap($div).find('input[data-module-name="ps_wirepayment"]').should('be.checked')
            }
        })
    }

    acceptTermsAndConditions(): void {
        this.elements.termsAndConditionsInput().click().should('be.checked')
    }

    confirmOrder(): void {
        this.elements.confirmOrderButton().click()
    }
}

export type ShippingMethod = 'test shoop' | 'My carrier'
export type PaymentMethod = 'check' | 'bank wire'

export default new OrderPage()