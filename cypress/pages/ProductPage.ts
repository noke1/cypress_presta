class ProductPage {

    elements = {
        quantityInput: () => cy.get('#quantity_wanted'),
        addProductToCartButton: () => cy.get('div.add button'),
        proceedToCheckoutButton: () => cy.contains('Proceed to checkout'),
    }

    selectQuantity(quantity: number): void {
        this.elements.quantityInput().clear().type(String(quantity))
    }

    addProductToCart(): void {
        this.elements.addProductToCartButton().click()
    }

    proceedToCheckout(): void {
        this.elements.proceedToCheckoutButton().click()
    }

}

export default new ProductPage()